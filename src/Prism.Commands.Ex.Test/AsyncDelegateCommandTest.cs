﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using FluentAssertions;
using Xunit;

namespace Prism.Commands.Ex.Test
{
    public class AsyncDelegateCommandTest
    {
        [Fact]
        public void CanExecuteTest()
        {
            // arrange
            var canExecuteCounter = 0;

            var target = new AsyncDelegateCommand(async (ct, progress) =>
            {
                await Task.Delay(1).ConfigureAwait(false);
            },
            () =>
            {
                canExecuteCounter++;
                return true;
            });
            ICommand targetAsICommand = target;

            // act
            target.CanExecute().Should().Be(true);
            targetAsICommand.CanExecute(null).Should().Be(true);

            // assert
            canExecuteCounter.Should().Be(2);
        }

        [Fact]
        public void CtorTest()
        {
            // act
            var target = new AsyncDelegateCommand((ct, progress) => Task.CompletedTask);

            // assert
            target.CanExecute().Should().BeTrue();
        }

        [Fact]
        public async void ExecuteExeptionTest()
        {
            // arrange
            var executeCounter = 0;

            var target = new AsyncDelegateCommand((ct, progress) => throw new Exception("purposely invoked."), () => true, a => executeCounter++);

            // act
            await target.ExecuteAsync().ConfigureAwait(false);

            // assert
            executeCounter.Should().Be(1);
        }

        [Fact]
        public async void ExecuteGlobalExeptionTest()
        {
            // arrange
            var executeCounter = 0;
            DelegateCommand.GlobalExceptionHandler = a => executeCounter++;

            var target = new AsyncDelegateCommand((ct, progress) => throw new Exception("purposely invoked."));

            // act
            await target.ExecuteAsync().ConfigureAwait(false);

            // assert
            executeCounter.Should().Be(1);
        }

        [Fact]
        public async void ExecuteTest()
        {
            // arrange
            var executeCounter = 0;

            var target = new AsyncDelegateCommand(async (ct, progress) =>
            {
                await Task.Delay(500).ConfigureAwait(false);
                executeCounter++;
            });

            // act
            await target.ExecuteAsync().ConfigureAwait(false);

            // assert
            executeCounter.Should().Be(1);
        }
    }
}