﻿using System;
using System.Windows.Input;
using FluentAssertions;
using Xunit;

namespace Prism.Commands.Ex.Test
{
    public class DelegateCommandGenericTest
    {
        [Fact]
        public void CanExecuteErrorTest()
        {
            // arrange
            var calledCounter = 0;

            // act
            var action = new Func<string, bool>((_) =>
            {
                calledCounter++;
                return calledCounter % 2 == 0;
            });
            ICommand target = new DelegateCommand<string>((_) => { }, action);
            target.CanExecute("foo").Should().BeFalse();
            target.CanExecute("foo").Should().BeTrue();

            // assert
            calledCounter.Should().Be(2);
        }

        [Fact]
        public void CanExecuteTest()
        {
            // arrange
            var calledCounter = 0;

            // act
            var action = new Func<string, bool>((_) =>
            {
                calledCounter++;
                return calledCounter % 2 == 0;
            });
            ICommand target = new DelegateCommand<string>((_) => { }, action);
            target.CanExecute("foo").Should().BeFalse();
            target.CanExecute("foo").Should().BeTrue();

            // assert
            calledCounter.Should().Be(2);
        }

        [Fact]
        public void ExecuteTest()
        {
            // arrange
            var calledCounter = 0;

            // act
            var action = new Action<string>((_) => calledCounter++);
            ICommand target = new DelegateCommand<string>(action);
            target.Execute("foo");

            // assert
            calledCounter.Should().Be(1);
        }
    }
}