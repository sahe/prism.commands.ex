﻿using System;
using System.Windows.Input;
using FluentAssertions;
using Xunit;

namespace Prism.Commands.Ex.Test
{
    public class DelegateCommandTest
    {
        [Fact]
        public void CanExecuteErrorTest()
        {
            // arrange
            var calledCounter = 0;

            // act
            var action = new Func<bool>(() =>
            {
                calledCounter++;
                return calledCounter % 2 == 0;
            });
            ICommand target = new DelegateCommand(() => { }, action);
            target.CanExecute(null).Should().BeFalse();
            target.CanExecute(null).Should().BeTrue();

            // assert
            calledCounter.Should().Be(2);
        }

        [Fact]
        public void CanExecuteTest()
        {
            // arrange
            var calledCounter = 0;

            // act
            var action = new Func<bool>(() =>
            {
                calledCounter++;
                return calledCounter % 2 == 0;
            });
            ICommand target = new DelegateCommand(() => { }, action);
            target.CanExecute(null).Should().BeFalse();
            target.CanExecute(null).Should().BeTrue();

            // assert
            calledCounter.Should().Be(2);
        }

        [Fact]
        public void ExecuteTest()
        {
            // arrange
            var calledCounter = 0;

            // act
            var action = new Action(() => calledCounter++);
            ICommand target = new DelegateCommand(action);
            target.Execute(null);

            // assert
            calledCounter.Should().Be(1);
        }
    }
}