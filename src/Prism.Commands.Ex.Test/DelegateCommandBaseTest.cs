﻿using System;
using FluentAssertions;
using Xunit;

namespace Prism.Commands.Ex.Test
{
    public class DelegateCommandBaseTest
    {
        [Fact]
        public void CastToGenericTest()
        {
            DelegateCommandBase.CastTo<int?>("123").Should().Be(123);
            DelegateCommandBase.CastTo<int?>(123.123d).Should().Be(123);
            DelegateCommandBase.CastTo<int?>("invalid-input").Should().Be(null);

            DelegateCommandBase.CastTo<double?>("123").Should().Be(123d);
            DelegateCommandBase.CastTo<double?>(123).Should().Be(123d);
            DelegateCommandBase.CastTo<double?>((decimal)123).Should().Be(123d);
            DelegateCommandBase.CastTo<double?>(123.123.ToString()).Should().Be(123.123d);
            DelegateCommandBase.CastTo<double?>("invalid-input").Should().Be(null);
        }
    }
}