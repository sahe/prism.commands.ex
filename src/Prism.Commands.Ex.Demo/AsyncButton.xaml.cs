﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace Prism.Commands.Ex.Demo
{
    /// <summary>
    /// Interaction logic for AsyncButton.xaml
    /// </summary>
    public partial class AsyncButton : UserControl
    {
        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CommandProperty = ButtonBase.CommandProperty.AddOwner(typeof(AsyncButton));

        public AsyncButton()
        {
            InitializeComponent();
        }

        public ICommand Command
        {
            get { return (ICommand)GetValue(ButtonBase.CommandProperty); }
            set { SetValue(ButtonBase.CommandProperty, value); }
        }
    }
}