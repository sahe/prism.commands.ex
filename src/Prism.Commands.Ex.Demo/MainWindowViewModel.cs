﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Prism.Commands.Ex.Demo
{
    public class MainWindowViewModel
    {
        public MainWindowViewModel()
        {
            AsyncCommand = new AsyncDelegateCommand(AsyncExecute);
            GenericAsyncCommand = new AsyncDelegateCommand<string>(GenericAsyncExecute);
            DelegateCommand = new DelegateCommand(DelegateExecute);
            GenericDelegateCommand = new DelegateCommand<string>(GenericDelegateExecute);
            IntDelegateCommand = new DelegateCommand<int>(IntDelegateExecute);
        }

        public AsyncDelegateCommand AsyncCommand { get; }

        public DelegateCommand DelegateCommand { get; set; }

        public AsyncDelegateCommand<string> GenericAsyncCommand { get; }

        public DelegateCommand<string> GenericDelegateCommand { get; set; }

        public DelegateCommand<int> IntDelegateCommand { get; set; }

        private async Task AsyncExecute(IProgress<int> progress, CancellationToken ct)
        {
            for (var i = 0; i < 100; i++)
            {
                await Task.Delay(100, ct);
                progress.Report(i);
                ct.ThrowIfCancellationRequested();
            }

            MessageBox.Show("done");
        }

        private async void DelegateExecute()
        {
            // Button is not disabled
            // Task.Delay(1000).Wait();
            await Task.Delay(1000);
            MessageBox.Show("done");
        }

        private async Task GenericAsyncExecute(string delay, IProgress<int> progress, CancellationToken ct)
        {
            for (var i = 0; i < 100; i++)
            {
                await Task.Delay(int.Parse(delay) / 100, ct);
                progress.Report(i);
                ct.ThrowIfCancellationRequested();
            }

            MessageBox.Show("done");
        }

        private void GenericDelegateExecute(string delay)
        {
            // Button is not disabled
            Task.Delay(int.Parse(delay)).Wait();
            MessageBox.Show("done");
        }

        private void IntDelegateExecute(int i)
        {
            MessageBox.Show($"Received an int parameter: {i}");
        }
    }
}