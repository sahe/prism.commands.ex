﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Windows.Input;

namespace Prism.Commands.Ex
{
    /// <summary>
    /// An <see cref="ICommand"/> whose delegates can be attached for <see cref="Execute(T)"/> and <see cref="CanExecute(T)"/>.
    /// </summary>
    /// <typeparam name="T">Parameter type.</typeparam>
    /// <remarks>
    /// The constructor deliberately prevents the use of value types.
    /// Because ICommand takes an object, having a value type for T would cause unexpected behavior when CanExecute(null) is called during XAML initialization for command bindings.
    /// Using default(T) was considered and rejected as a solution because the implementor would not be able to distinguish between a valid and defaulted values.
    /// <para/>
    /// Instead, callers should support a value type by using a nullable value type and checking the HasValue property before using the Value property.
    /// <example>
    ///     <code>
    /// public MyClass()
    /// {
    ///     this.submitCommand = new DelegateCommand&lt;int?&gt;(this.Submit, this.CanSubmit);
    /// }
    ///
    /// private bool CanSubmit(int? customerId)
    /// {
    ///     return (customerId.HasValue &amp;&amp; customers.Contains(customerId.Value));
    /// }
    ///     </code>
    /// </example>
    /// </remarks>
    public class DelegateCommand<T> : DelegateCommandBase
    {
        private readonly Action<Exception> _exceptionMethod;

        private readonly Action<T> _executeMethod;

        private Func<T, bool> _canExecuteMethod;

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommand{T}"/> class.
        /// </summary>
        /// <param name="executeMethod">Delegate to execute when Execute is called on the command. This can be null to just hook up a CanExecute delegate.</param>
        /// <remarks><see cref="CanExecute(T)"/> will always return true.</remarks>
        public DelegateCommand(Action<T> executeMethod)
            : this(executeMethod, (_) => true)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommand{T}"/> class.
        /// </summary>
        /// <param name="executeMethod">Delegate to execute when Execute is called on the command. This can be null to just hook up a CanExecute delegate.</param>
        /// <remarks><see cref="CanExecute(T)"/> will always return true.</remarks>
        /// /// <param name="canExecuteMethod">Delegate to execute when CanExecute is called on the command. This can be null.</param>
        public DelegateCommand(Action<T> executeMethod, Func<T, bool> canExecuteMethod)
            : this(executeMethod, canExecuteMethod, exceptionMethod: null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommand{T}"/> class.
        /// </summary>
        /// <param name="executeMethod">Delegate to execute when Execute is called on the command. This can be null to just hook up a CanExecute delegate.</param>
        /// <param name="canExecuteMethod">Delegate to execute when CanExecute is called on the command. This can be null.</param>
        /// <param name="exceptionMethod">The <see cref="Action"/> that is called in case <paramref name="executeMethod"/> or <paramref name="canExecuteMethod"/> throws an exception.</param>
        /// <exception cref="ArgumentNullException">When both <paramref name="executeMethod"/> and <paramref name="canExecuteMethod"/> are <see langword="null" />.</exception>
        public DelegateCommand(Action<T> executeMethod, Func<T, bool> canExecuteMethod, Action<Exception> exceptionMethod)
        {
            if (executeMethod == null || canExecuteMethod == null)
                throw new ArgumentNullException(nameof(executeMethod), "Neither the executeMethod nor the canExecuteMethod delegates can be null.");

            // DelegateCommand allows object or Nullable<>.
            var genericTypeInfo = typeof(T).GetTypeInfo();
            if (genericTypeInfo.IsValueType && (!typeof(Nullable<>).GetTypeInfo().IsAssignableFrom(genericTypeInfo.GetGenericTypeDefinition().GetTypeInfo())))
            {
                throw new InvalidCastException($"Value types are not supported by {nameof(DelegateCommand<T>)}. Use Nullable<T> instead.");
            }

            _executeMethod = executeMethod;
            _canExecuteMethod = canExecuteMethod;
            _exceptionMethod = exceptionMethod;

            // There is no use, for the UI Thread cannot render button while being busy executing the command.
            // If the command uses any async operation, DelegateCommand is not aware of it and already setting IsActive to false.
            // ObservesProperty(() => IsActive); // Updates its own CanExecuteChanged on change of IsActive
        }

        /// <summary>
        /// Determines if the command can be executed.
        /// </summary>
        /// <returns>Returns <see langword="true"/> if the command can execute,otherwise returns <see langword="false"/>.</returns>
        public bool CanExecute(T parameter) => CanExecute(parameter as object);

        /// <summary>
        /// Executes the command and invokes the <see cref="Action{T}"/> provided during construction.
        /// </summary>
        /// <param name="parameter">Data used by the command.</param>
        public void Execute(T parameter) => Execute(parameter as object);

        /// <summary>
        /// Observes a property that is used to determine if this command can execute, and if it implements INotifyPropertyChanged it will automatically call DelegateCommandBase.RaiseCanExecuteChanged on property changed notifications.
        /// </summary>
        /// <param name="canExecuteExpression">The property expression. Example: ObservesCanExecute(() => PropertyName).</param>
        /// <returns>The current instance of DelegateCommand.</returns>
        public DelegateCommand<T> ObservesCanExecute(Expression<Func<bool>> canExecuteExpression)
        {
            var expression = Expression.Lambda<Func<T, bool>>(canExecuteExpression.Body, Expression.Parameter(typeof(T), "o"));
            _canExecuteMethod = expression.Compile();
            ObservesPropertyInternal(canExecuteExpression);
            return this;
        }

        /// <summary>
        /// Observes a property that implements INotifyPropertyChanged, and automatically calls DelegateCommandBase.RaiseCanExecuteChanged on property changed notifications.
        /// </summary>
        /// <typeparam name="TType">The type of the return value of the method that this delegate encapulates.</typeparam>
        /// <param name="propertyExpression">The property expression. Example: ObservesProperty(() => PropertyName).</param>
        /// <returns>The current instance of DelegateCommand.</returns>
        public DelegateCommand<T> ObservesProperty<TType>(Expression<Func<TType>> propertyExpression)
        {
            ObservesPropertyInternal(propertyExpression);
            return this;
        }

        /// <summary>
        /// Handle the internal invocation of <see cref="ICommand.CanExecute(object)"/>.
        /// </summary>
        /// <returns><see langword="true"/> if the Command Can Execute, otherwise <see langword="false" />.</returns>
        protected override bool CanExecute(object parameter)
        {
            try
            {
                // deal with reference type
                return !IsActive
                    && _canExecuteMethod(CastTo<T>(parameter));
            }
            catch (Exception)
            {
                // CanExecute sometimes is called with null, just because Bindings have not yet updated.
                return false;
            }
        }

        /// <summary>
        /// Handle the internal invocation of <see cref="ICommand.Execute(object)"/>.
        /// </summary>
        /// <param name="parameter">Command Parameter.</param>
        protected override void Execute(object parameter)
        {
            try
            {
                IsActive = true;
                _executeMethod(CastTo<T>(parameter));
            }
            catch (Exception ex)
            {
                if (_exceptionMethod != null)
                    _exceptionMethod(ex);
                else if (DelegateCommandBase.GlobalExceptionHandler != null)
                    DelegateCommandBase.GlobalExceptionHandler(ex);
                else
                    throw;
            }
            finally
            {
                IsActive = false;
            }
        }
    }
}