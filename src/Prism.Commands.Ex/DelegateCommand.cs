using System;
using System.Linq.Expressions;
using System.Windows.Input;

namespace Prism.Commands.Ex
{
    /// <summary>
    /// An <see cref="ICommand"/> whose delegates do not take any parameters for <see cref="Execute()"/> and <see cref="CanExecute()"/>.
    /// </summary>
    /// <see cref="DelegateCommandBase"/>
    /// <see cref="DelegateCommand{T}"/>
    public class DelegateCommand : DelegateCommandBase
    {
        private readonly Action<Exception> _exceptionMethod;

        private readonly Action _executeMethod;

        private Func<bool> _canExecuteMethod;

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommand"/> class.
        /// Creates a new instance of <see cref="DelegateCommand"/> with the <see cref="Action"/> to invoke on execution.
        /// </summary>
        /// <param name="executeMethod">The <see cref="Action"/> to invoke when <see cref="ICommand.Execute(object)"/> is called.</param>
        public DelegateCommand(Action executeMethod)
            : this(executeMethod, () => true)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommand"/> class.
        /// Creates a new instance of <see cref="DelegateCommand"/> with the <see cref="Action"/> to invoke on execution.
        /// </summary>
        /// <param name="executeMethod">The <see cref="Action"/> to invoke when <see cref="ICommand.Execute(object)"/> is called.</param>
        /// /// <param name="canExecuteMethod">The <see cref="Func{TResult}"/> to invoke when <see cref="ICommand.CanExecute"/> is called.</param>
        public DelegateCommand(Action executeMethod, Func<bool> canExecuteMethod)
            : this(executeMethod, canExecuteMethod, exceptionMethod: null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommand"/> class.
        /// Creates a new instance of <see cref="DelegateCommand"/> with the <see cref="Action"/> to invoke on execution
        /// and a <see langword="Func" /> to query for determining if the command can execute.
        /// </summary>
        /// <param name="executeMethod">The <see cref="Action"/> to invoke when <see cref="ICommand.Execute"/> is called.</param>
        /// <param name="canExecuteMethod">The <see cref="Func{TResult}"/> to invoke when <see cref="ICommand.CanExecute"/> is called.</param>
        /// <param name="exceptionMethod">The <see cref="Action"/> that is called in case <paramref name="executeMethod"/> or <paramref name="canExecuteMethod"/> throws an exception.</param>
        public DelegateCommand(Action executeMethod, Func<bool> canExecuteMethod, Action<Exception> exceptionMethod)
        {
            if (executeMethod == null || canExecuteMethod == null)
                throw new ArgumentNullException(nameof(executeMethod), "Neither the executeMethod nor the canExecuteMethod delegates can be null.");

            _executeMethod = executeMethod;
            _canExecuteMethod = canExecuteMethod;
            _exceptionMethod = exceptionMethod;

            // There is no use, for the UI Thread cannot render button while being busy executing the command.
            // If the command uses any async operation, DelegateCommand is not aware of it and already setting IsActive to false.
            // ObservesProperty(() => IsActive); // Updates its own CanExecuteChanged on change of IsActive
        }

        /// <summary>
        /// Determines if the command can be executed.
        /// </summary>
        /// <returns>Returns <see langword="true"/> if the command can execute,otherwise returns <see langword="false"/>.</returns>
        public bool CanExecute() => CanExecute(null);

        /// <summary>
        /// Executes the command.
        /// </summary>
        public void Execute() => Execute(null);

        /// <summary>
        /// Observes a property that is used to determine if this command can execute, and if it implements INotifyPropertyChanged it will automatically call DelegateCommandBase.RaiseCanExecuteChanged on property changed notifications.
        /// </summary>
        /// <param name="canExecuteExpression">The property expression. Example: ObservesCanExecute(() => PropertyName).</param>
        /// <returns>The current instance of DelegateCommand.</returns>
        public DelegateCommand ObservesCanExecute(Expression<Func<bool>> canExecuteExpression)
        {
            _canExecuteMethod = canExecuteExpression.Compile();
            ObservesPropertyInternal(canExecuteExpression);
            return this;
        }

        /// <summary>
        /// Observes a property that implements INotifyPropertyChanged, and automatically calls DelegateCommandBase.RaiseCanExecuteChanged on property changed notifications.
        /// </summary>
        /// <typeparam name="T">The object type containing the property specified in the expression.</typeparam>
        /// <param name="propertyExpression">The property expression. Example: ObservesProperty(() => PropertyName).</param>
        /// <returns>The current instance of DelegateCommand.</returns>
        public DelegateCommand ObservesProperty<T>(Expression<Func<T>> propertyExpression)
        {
            ObservesPropertyInternal(propertyExpression);
            return this;
        }

        /// <summary>
        /// Handle the internal invocation of <see cref="ICommand.CanExecute(object)"/>.
        /// </summary>
        /// <returns><see langword="true"/> if the Command Can Execute, otherwise <see langword="false" />.</returns>
        protected override bool CanExecute(object parameter)
        {
            try
            {
                return !IsActive
                    && _canExecuteMethod();
            }
            catch (Exception ex)
            {
                if (_exceptionMethod != null)
                    _exceptionMethod(ex);
                else if (DelegateCommandBase.GlobalExceptionHandler != null)
                    DelegateCommandBase.GlobalExceptionHandler(ex);
                else
                    throw;
            }

            return false;
        }

        /// <summary>
        /// Handle the internal invocation of <see cref="ICommand.Execute(object)"/>.
        /// </summary>
        /// <param name="parameter">Command Parameter.</param>
        protected override void Execute(object parameter)
        {
            try
            {
                IsActive = true;
                _executeMethod();
            }
            catch (Exception ex)
            {
                if (_exceptionMethod != null)
                    _exceptionMethod(ex);
                else if (DelegateCommandBase.GlobalExceptionHandler != null)
                    DelegateCommandBase.GlobalExceptionHandler(ex);
                else
                    throw;
            }
            finally
            {
                IsActive = false;
            }
        }
    }
}