using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
using System.Threading;
using System.Windows.Input;
using PropertyChanged;

namespace Prism.Commands.Ex
{
    /// <summary>
    /// An <see cref="ICommand"/> whose delegates can be attached for <see cref="Execute"/> and <see cref="CanExecute"/>.
    /// </summary>
    public abstract class DelegateCommandBase : NotifyPropertyChanged, ICommand, IActiveAware
    {
        private readonly HashSet<string> _observedPropertiesExpressions = new HashSet<string>();

        private readonly SynchronizationContext _synchronizationContext;

        private bool _isActive;

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommandBase"/> class.
        /// Creates a new instance of a <see cref="DelegateCommandBase"/>, specifying both the execute action and the can execute function.
        /// </summary>
        protected DelegateCommandBase()
        {
            _synchronizationContext = SynchronizationContext.Current;
        }

        /// <summary>
        /// Occurs when changes occur that affect whether or not the command should execute.
        /// </summary>
        public virtual event EventHandler CanExecuteChanged;

        /// <summary>
        /// Fired if the <see cref="IsActive"/> property changes.
        /// </summary>
        public virtual event EventHandler IsActiveChanged;

        /// <summary>
        /// Gets or sets an action that is invoked if any given <see cref="Execute(object)"/> or <see cref="CanExecute(object)"/>
        /// delegate throws an exception.
        /// If not set (null by default), the exception is re-thrown.
        /// </summary>
        public static Action<Exception> GlobalExceptionHandler { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the object is active.
        /// </summary>
        /// <value><see langword="true" /> if the object is active; otherwise <see langword="false" />.</value>
        public bool IsActive
        {
            get
            {
                return _isActive;
            }

            set
            {
                if (_isActive != value)
                {
                    _isActive = value;
                    OnIsActiveChanged();
                    OnPropertyChanged(nameof(IsActive));
                }
            }
        }

        public static T CastTo<T>(object parameter)
        {
            if (Nullable.GetUnderlyingType(typeof(T)) != null)
            {
                T retval;

                try
                {
                    // Attempt to convert to nullable
                    // This supports e.g. casting from string to int?.
                    var conv = TypeDescriptor.GetConverter(typeof(T));
                    retval = (T)conv.ConvertFrom(parameter);
                }
                catch (Exception)
                {
                    try
                    {
                        // Attempt to convert to generic type of nullable
                        // This supports e.g. casting from double to int?.
                        retval = (T)Convert.ChangeType(parameter, typeof(T).GenericTypeArguments[0]);
                    }
                    catch (Exception)
                    {
                        retval = default;
                    }
                }

                return retval;
            }
            else
            {
                // deal with reference type
                return (T)parameter;
            }
        }

        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute(parameter);
        }

        void ICommand.Execute(object parameter)
        {
            Execute(parameter);
        }

        /// <summary>
        /// Raises <see cref="CanExecuteChanged"/> so every command invoker
        /// can re-query to check if the command can execute.
        /// <remarks>Note that this will trigger the execution of <see cref="CanExecuteChanged"/> once for each invoker.</remarks>
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1030:UseEventsWhereAppropriate", Justification = "by Prism")]
        public void RaiseCanExecuteChanged()
        {
            OnCanExecuteChanged();
        }

        /// <summary>
        /// Observes a property that implements INotifyPropertyChanged, and automatically calls DelegateCommandBase.RaiseCanExecuteChanged on property changed notifications.
        /// </summary>
        /// <typeparam name="T">The object type containing the property specified in the expression.</typeparam>
        /// <param name="propertyExpression">The property expression. Example: ObservesProperty(() => PropertyName).</param>
        protected internal void ObservesPropertyInternal<T>(Expression<Func<T>> propertyExpression)
        {
            if (_observedPropertiesExpressions.Contains(propertyExpression.ToString()))
            {
                throw new ArgumentException($"{propertyExpression} is already being observed.", nameof(propertyExpression));
            }
            else
            {
                _observedPropertiesExpressions.Add(propertyExpression.ToString());
                PropertyObserver.Observes(propertyExpression, RaiseCanExecuteChanged);
            }
        }

        /// <summary>
        /// Handle the internal invocation of <see cref="ICommand.CanExecute(object)"/>.
        /// </summary>
        /// <returns><see langword="true"/> if the Command Can Execute, otherwise <see langword="false" />.</returns>
        protected abstract bool CanExecute(object parameter);

        /// <summary>
        /// Handle the internal invocation of <see cref="ICommand.Execute(object)"/>.
        /// </summary>
        /// <param name="parameter">Command Parameter.</param>
        protected abstract void Execute(object parameter);

        /// <summary>
        /// Raises <see cref="ICommand.CanExecuteChanged"/> so every
        /// command invoker can re-query <see cref="ICommand.CanExecute"/>.
        /// </summary>
        [SuppressPropertyChangedWarnings]
        protected virtual void OnCanExecuteChanged()
        {
            var handler = CanExecuteChanged;
            if (handler != null)
            {
                if (_synchronizationContext != null && _synchronizationContext != SynchronizationContext.Current)
                    _synchronizationContext.Post((_) => handler.Invoke(this, EventArgs.Empty), null);
                else
                    handler.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// This raises the <see cref="IsActiveChanged"/> event.
        /// </summary>
        protected virtual void OnIsActiveChanged()
        {
            IsActiveChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}