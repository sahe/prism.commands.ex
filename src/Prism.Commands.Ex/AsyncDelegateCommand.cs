using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Prism.Commands.Ex
{
    /// <summary>
    /// An <see cref="ICommand"/> whose delegates do not take any parameters for <see cref="ExecuteAsync()"/> and <see cref="CanExecute()"/>.
    /// </summary>
    /// <see cref="DelegateCommandBase"/>
    /// <see cref="AsyncDelegateCommand{T}"/>
    public sealed class AsyncDelegateCommand : DelegateCommandBase
    {
        private readonly Action<Exception> _exceptionMethod;

        private readonly Func<IProgress<int>, CancellationToken, Task> _executeMethod;

        private readonly Progress<int> _progress;

        private CancellationTokenSource _cancellationTokenSource;

        private Func<bool> _canExecuteMethod;

        /// <summary>
        /// Initializes a new instance of the <see cref="AsyncDelegateCommand"/> class.
        /// Creates a new instance of <see cref="AsyncDelegateCommand"/> with the <see cref="Action"/> to invoke on execution.
        /// </summary>
        /// <param name="executeMethod">The <see cref="Action"/> to invoke when <see cref="ICommand.Execute(object)"/> is called.</param>
        public AsyncDelegateCommand(Func<IProgress<int>, CancellationToken, Task> executeMethod)
            : this(executeMethod, () => true)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AsyncDelegateCommand"/> class.
        /// Creates a new instance of <see cref="AsyncDelegateCommand"/> with the <see cref="Action"/> to invoke on execution.
        /// </summary>
        /// <param name="executeMethod">The <see cref="Action"/> to invoke when <see cref="ICommand.Execute(object)"/> is called.</param>
        /// <param name="canExecuteMethod">The <see cref="Func{TResult}"/> to invoke when <see cref="ICommand.CanExecute"/> is called.</param>
        public AsyncDelegateCommand(Func<IProgress<int>, CancellationToken, Task> executeMethod, Func<bool> canExecuteMethod)
            : this(executeMethod, canExecuteMethod, exceptionMethod: null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AsyncDelegateCommand"/> class.
        /// Creates a new instance of <see cref="AsyncDelegateCommand"/> with the <see cref="Action"/> to invoke on execution
        /// and a <see langword="Func" /> to query for determining if the command can execute.
        /// </summary>
        /// <param name="executeMethod">The <see cref="Action"/> to invoke when <see cref="ICommand.Execute"/> is called.</param>
        /// <param name="canExecuteMethod">The <see cref="Func{TResult}"/> to invoke when <see cref="ICommand.CanExecute"/> is called.</param>
        /// <param name="exceptionMethod">The <see cref="Action"/> that is called in case <paramref name="executeMethod"/> or <paramref name="canExecuteMethod"/> throws an exception.</param>
        public AsyncDelegateCommand(Func<IProgress<int>, CancellationToken, Task> executeMethod, Func<bool> canExecuteMethod, Action<Exception> exceptionMethod)
        {
            if (executeMethod == null || canExecuteMethod == null)
                throw new ArgumentNullException(nameof(executeMethod), "Neither the executeMethod nor the canExecuteMethod delegates can be null.");

            _executeMethod = executeMethod;
            _canExecuteMethod = canExecuteMethod;
            ObservesProperty(() => IsActive); // Updates its own CanExecuteChanged on change of IsActive
            _exceptionMethod = exceptionMethod;

            _progress = new Progress<int>();
            _progress.ProgressChanged += (_, newValue) => Progress = newValue;

            Cancel = new DelegateCommand(() => _cancellationTokenSource.Cancel(), () => IsActive).ObservesProperty(() => IsActive);
        }

        public DelegateCommand Cancel { get; }

        public int Progress { get; set; } // Public set is not required, but it makes XAML cleaner, for Mode OneWay does not have to be specified.

        /// <summary>
        /// Determines if the command can be executed.
        /// </summary>
        /// <returns>Returns <see langword="true"/> if the command can execute,otherwise returns <see langword="false"/>.</returns>
        public bool CanExecute() => CanExecute(null);

        /// <summary>
        /// Executes the command.
        /// </summary>
        public async Task ExecuteAsync()
        {
            try
            {
                _cancellationTokenSource = new CancellationTokenSource();

                ResetProgress();

                IsActive = true;
                await _executeMethod(_progress, _cancellationTokenSource.Token);
            }
            catch (Exception ex)
            {
                if (_exceptionMethod != null)
                    _exceptionMethod(ex);
                else if (DelegateCommandBase.GlobalExceptionHandler != null)
                    DelegateCommandBase.GlobalExceptionHandler(ex);
                else
                    throw;
            }
            finally
            {
                IsActive = false;
                ResetProgress();
            }
        }

        /// <summary>
        /// Observes a property that is used to determine if this command can execute, and if it implements INotifyPropertyChanged it will automatically call DelegateCommandBase.RaiseCanExecuteChanged on property changed notifications.
        /// </summary>
        /// <param name="canExecuteExpression">The property expression. Example: ObservesCanExecute(() => PropertyName).</param>
        /// <returns>The current instance of DelegateCommand.</returns>
        public AsyncDelegateCommand ObservesCanExecute(Expression<Func<bool>> canExecuteExpression)
        {
            _canExecuteMethod = canExecuteExpression.Compile();
            ObservesPropertyInternal(canExecuteExpression);
            return this;
        }

        /// <summary>
        /// Observes a property that implements INotifyPropertyChanged, and automatically calls DelegateCommandBase.RaiseCanExecuteChanged on property changed notifications.
        /// </summary>
        /// <typeparam name="T">The object type containing the property specified in the expression.</typeparam>
        /// <param name="propertyExpression">The property expression. Example: ObservesProperty(() => PropertyName).</param>
        /// <returns>The current instance of DelegateCommand.</returns>
        public AsyncDelegateCommand ObservesProperty<T>(Expression<Func<T>> propertyExpression)
        {
            ObservesPropertyInternal(propertyExpression);
            return this;
        }

        /// <summary>
        /// Handle the internal invocation of <see cref="ICommand.CanExecute(object)"/>.
        /// </summary>
        /// <returns><see langword="true"/> if the Command Can Execute, otherwise <see langword="false" />.</returns>
        protected override bool CanExecute(object parameter)
        {
            try
            {
                return !IsActive
                    && _canExecuteMethod();
            }
            catch (Exception ex)
            {
                if (_exceptionMethod != null)
                    _exceptionMethod(ex);
                else if (DelegateCommandBase.GlobalExceptionHandler != null)
                    DelegateCommandBase.GlobalExceptionHandler(ex);
                else
                    throw;
            }

            return false;
        }

        /// <summary>
        /// Handle the internal invocation of <see cref="ICommand.Execute(object)"/>.
        /// </summary>
        /// <param name="parameter">Command Parameter.</param>
        protected override async void Execute(object parameter)
        {
            try
            {
                await ExecuteAsync();
            }
            catch (TaskCanceledException)
            {
                // ignore, user must have canceled it purposely.
            }
        }

        private void ResetProgress()
        {
            (_progress as IProgress<int>).Report(0);
        }
    }
}